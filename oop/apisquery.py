import requests, json

from config.countries import apis_by_countrys

class ApisQuery:
    def summary(self, slug):
        for item in apis_by_countrys:
            if item['slug'] == slug:
                country = item['country']
                flag = item['flag']
                api_url = item['api_url'] if item['unique'] else 'https://api.covid19api.com/summary'
                summary_fields = item['fields']['summary'] if item['unique'] else None
                unique = item['unique']
                handled = True
                break
        else:
            handled = False

        data_summary = {}
        if handled:
            response = requests.get(api_url)
            data = response.json()
            if unique:
                for key, value in summary_fields.items():
                    data_summary[key] = int(data[value]) if slug != 'venezuela' else data[value]['Count']
            else:
                for countries in data['Countries']:
                    if slug == countries['Slug']:
                        data_summary['confirmed'] = countries['TotalConfirmed'] + countries['NewConfirmed']
                        data_summary['recovered'] = countries['TotalRecovered'] + countries['NewRecovered']
                        data_summary['deaths'] = countries['TotalDeaths'] + countries['NewDeaths']

            data_summary['flag'] = flag
            data_summary['country'] = country
            data_summary['active'] = data_summary['confirmed'] - data_summary['recovered'] - data_summary['deaths']

        data_summary['handled'] = handled
        return data_summary

    def state(self, slug):
        for countries in apis_by_countrys:
            if countries['slug'] == slug:
                if countries['unique'] and countries['fields']['state']:
                    country = countries['country']
                    flag = countries['flag']
                    api_url = countries['api_url']
                    state_fields = countries['fields']['state']
                    handled = True
                    break
        else:
            handled = False

        all_states = {
            'states': ''
        }
        if handled:
            response = requests.get(api_url)
            data = response.json()
            container = data[state_fields['container']] if slug != 'venezuela' else data['Confirmed']['ByState']
            states = ""
            if type(container) is dict:
                for state, cases in container.items():
                    all_states['states'] += f"\n{state}: {cases}"
            else:
                for index, dictionary in enumerate(container):
                    if index == state_fields['jump_position']:
                        continue
                    all_states['states'] += f"\n{dictionary[state_fields['state_name']]}: {dictionary[state_fields['total_cases']]}"
            all_states['country'] = country
            all_states['state_or_province'] = state_fields['state_or_province']
            all_states['flag'] = flag

        all_states['handled'] = handled
        return all_states