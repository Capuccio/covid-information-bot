# Covid Information Bot

Telegram Bot developed with Python to inform people about case summary in several countries and their states/provinces

## Setting Up

### Installation

Run in Command Terminal

```
> pip install -r requirements.txt
```

### Config

In **config/auth.py** path you can set your Telegram Bot API Token

## Start

### Python command

**After setting up** the project, **run** the next command.

```
> python main.py
```

## Authors

- **Jose Cordova** - _Capuccio_ - Software Developer 👨‍💻
- **Jose Vicente** - _Chentrovski_ - Graphic Designer 👨‍🎨
