import unicodedata

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, Filters, MessageHandler, ConversationHandler
from telegram.error import TelegramError, Unauthorized, BadRequest, TimedOut, ChatMigrated, NetworkError

import logging

from oop.apisquery import ApisQuery
from config.auth import TOKEN

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger('Covid Report')

query_class = ApisQuery()

def logger_print(command, message):
    print("|----------------|")
    logger.info('He recibido el comando /{0} del chat: {1}'.format(command, message['chat']['id']))
    print("|----------------|")
    print(message['from_user']['language_code'])
    return

def start(update, context):
    logger_print('start', update.message)

    context.bot.send_message(
        chat_id = update.message.chat_id,
        text = "🤖 ¡Hola! Soy Covid Information, un Bot que te dará la información que necesites sobre el COVID-19. 👑🦠"
    )

def choice_type_information(update, context):
    logger_print('informacion', update.message)

    keyboard = [[InlineKeyboardButton("COVID-19", callback_data='info_1')],
                [InlineKeyboardButton("Síntomas", callback_data='info_2')],
                [InlineKeyboardButton("Transmisión", callback_data='info_3')],
                [InlineKeyboardButton("Prevención", callback_data='info_4')]]

    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text('¿Que información desea saber?', reply_markup = reply_markup)

def call_back_type_information(update, context):
    call_back_query = update.callback_query
    option_selected = call_back_query.data

    if option_selected == 'info_1':
        string_answer = """👑 *COVID-19* 🦠
        \nEl *COVID-19* también conocido como *enfermedad por coronavirus* 👑🦠 o, incorrectamente, como *neumonía por coronavirus*, es una *enfermedad infecciosa* causada por el virus *SARS-CoV-2*.
        \nSe detectó por primera vez en la ciudad China 🇨🇳 de *Wuhan* (_provincia de Hubei_) en diciembre de 2019. Habiendo llegado a más de 100 territorios, el 11 de marzo de 2020 la Organización Mundial de la Salud (_OMS_) la declaró pandemia ☣."""

    elif option_selected == 'info_2':
        string_answer = """🤒 *Síntomas* 🤕
        \nProduce síntomas similares a los de la gripe, entre los que incluye *fiebre*, *tos seca*, *dificultad para respirar*, *dolor muscular* y *fatiga* .
        \nEn casos graves produce *neumonía*, *síndrome de dificultad respiratoria aguda*, *sepsis* y *choque séptico* que conduce a alrededor del 3% de los infectados a la muerte. No existe tratamiento específico; las medidas terapéuticas principales consisten en aliviar los síntomas y mantener las funciones vitales.
        \n\n⚠ _Se recomienda acudir al médico si presenta fiebre, tos y dificultad para respirar_"""

    elif option_selected == 'info_3':
        string_answer = """🤔 *Transmisión* 🤨
        \nLa transmisión del *SARS-CoV-2* 🦠 se produce mendiante pequeñas gotas 💦 (_microgotas de Flügge_) que se emiten al hablar 🗣, estornudar 🤧 o exhalar 👃, que al ser despedidas por un portador (_que puede no tener síntomas de la enfermedad o estar incubándola_) pasan directamente a otra persona mediante la inhalación 👃, o quedan sobre los objetos 📱💻 y superficies que rodean al emisor 🗄📦, y luego, a través de las manos 🖐, que lo recogen del ambiente contaminado, toman contacto con las membranas mucosas orales, nasales y oculares, al tocarse la boca 👄, la nariz 👃 o los ojos 👁. Esta última es la principal vía de propagación, ya que el virus puede permanecer viable hasta por días en los *fómites* 🛏 💵📱👜 (_cualquier objeto carente de vida, o sustancia, que si se contamina con algún patógeno es capaz de transferirlo de un individuo a otro_)"""

    elif option_selected == 'info_4':
        string_answer = """✅ *Prevención* 🚫
        \n🚫🤧🤲 *No estornude en sus manos*.\n🚫🤝 *No dar apretón de mano*.\n🚫🏟 *Evitar reuniones con gente*.\n🚫🤦 *Evitar tocarse los ojos, la nariz o la boca con las manos sin lavar*.
        \n✅🤧💪 *estornude en su codo*.\n✅👨↔👩 *Distancia de 1m* (_3 pies_).\n✅👪🏠 *Quedarse en casa*.\n✅🧼🤲🚰 *Lavarse las manos a menudo con agua y jabón durante 20 segundos*.
        \n😷 _Si sospecha que está infectado debe usar una mascarilla quirúrgica_.\n🛒😠 _Cuando vayas a comprar, no compres literalmente todo, deja algo para el resto también_.
        \n\n⚠ _No hay evidencia que demuestre que las máscaras protegen a las personas no infectadas con bajo riesgo, y usarlas puede crear una falsa sensación de seguridad_"""

    call_back_query.edit_message_text(
        parse_mode = 'Markdown',
        text = f"{string_answer}"
    )

def summary(update, context):
    logger_print('total', update.message)

    text = tuple(update['message']['text'].split())
    slug = "-".join(text[1:]).lower() if len(text) >= 2 else None

    if slug == None:
        answer = """No he recibido el país a consultar. Tenemos información de los siguientes países:
        \n🇨🇴 Colombia\n🇨🇷 Costa Rica\n🇪🇸 España\n🇮🇹 Italia\n🇲🇽 México\n🇵🇪 Perú\n🇻🇪 Venezuela\n\n_Ej: /total México_"""
    else:
        slug = unicodedata.normalize('NFD', slug)\
                    .encode('ASCII', 'ignore')\
                    .decode('utf-8')

        DATA_SUMMARY = query_class.summary(slug)

        if DATA_SUMMARY['handled']:
            answer = f"Resumen de Casos en *{DATA_SUMMARY['country']}* {DATA_SUMMARY['flag']}:\n\n✅ Confirmados: {DATA_SUMMARY['confirmed']}\n😁 Recuperados: {DATA_SUMMARY['recovered']}\n⚰ Fallecidos: {DATA_SUMMARY['deaths']}\n😷 Activos: {DATA_SUMMARY['active']}"
        else:
            answer = f"🙇‍♂️ Todavía no manejamos información sobre ese país."

    context.bot.send_message(
        parse_mode = 'Markdown',
        chat_id = update.message.chat_id,
        text = f"{answer}"
    )

def state(update, context):
    logger_print('estados', update.message)

    text = tuple(update['message']['text'].split())
    slug = "-".join(text[1:]).lower() if len(text) >= 2 else None

    if slug == None:
        answer = f"""No he recibido el nombre de un país para mostrar sus Estados o Provincias. Manejamos datos de los siguientes países:
        \n🇻🇪 Venezuela\n\n_Ejemplo: /estados Venezuela_
        \n_Si desea ver más Estados/Provincias de otros países, puede ayudarnos utilizando el comando /apoyo_."""
    else:
        slug = unicodedata.normalize('NFD', slug)\
                    .encode('ASCII', 'ignore')\
                    .decode('utf-8')
        all_states = query_class.state(slug)

        if all_states['handled']:
            answer = f"{all_states['state_or_province']} con casos de COVID-19 en {all_states['country']} {all_states['flag']} :\n{all_states['states']}"
        else:
            answer = f"🙇‍♂️ Todavía no manejamos los Estados o Provincias de ese país."

    context.bot.send_message(
        parse_mode = 'Markdown',
        chat_id = update.message.chat_id,
        text = f"{answer}"
    )

def support(update, context):
    logger_print('apoyo', update.message)

    keyboard = [[InlineKeyboardButton("🏦 PayPal", callback_data='support_paypal'), InlineKeyboardButton("🏦 Uphold", callback_data='support_uphold')],
                [InlineKeyboardButton("BTC", callback_data='support_btc'), InlineKeyboardButton("ETH", callback_data='support_eth'), InlineKeyboardButton("DASH", callback_data='support_dash'), InlineKeyboardButton("LTC", callback_data='support_ltc')]]

    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text("🙏 Su aporte puede ayudarnos a continuar mejorando funcionalidades del Bot y continuar añadiendo más países con sus estados/provincias.", reply_markup = reply_markup)

def support_call_back(update, context):
    call_back_query = update.callback_query
    option_selected = call_back_query.data

    if option_selected == 'support_paypal':
        answer = "covidinformationbot@gmail.com"
    elif option_selected == 'support_uphold':
        answer = "CovidInformation"
    elif option_selected == 'support_btc':
        answer = "1HAFkuF3nP7GW5viCEPRp2gUS6rLbGdRLj"
    elif option_selected == 'support_eth':
        answer = "0x30945Ef938692f0d9A066FD12247751D2c0b6a44"
    elif option_selected == 'support_dash':
        answer = "XyoEQc494XM2DhbQBZgxkhjPvoGnRzaTNJ"
    elif option_selected == 'support_ltc':
        answer = "LaZcGw9tGKw9QVCULmguy3AMmKFXvgqyHX"

    call_back_query.edit_message_text(
        parse_mode = 'Markdown',
        text = f"*{answer}*"
    )

def contact(update, context):
    logger_print('contacto', update.message)

    context.bot.send_message(
        parse_mode = 'Markdown',
        chat_id = update.message.chat_id,
        text = """Puede contactar con nosotros mediante nuestro correo electrónico para cualquier duda, aporte u otra cosa que necesite.
        \n✉ *covidinformationbot@gmail.com*"""
    )

def unknown(update, context):
    logger_print('unknown', update.message)

    context.bot.send_message(
        chat_id = update.message.chat_id,
        text = "🤔 Lo siento pero no entiendo ese comando, por favor revisa nuestra lista de comandos e intenta nuevamente."
    )

def error_handler(update, context):
    try:
        raise context.error
    except Unauthorized:
        logger.warning(f"Error Unauthorized")
        # remove update.message.chat_id from conversation list
    except BadRequest:
        logger.warning(f"Error BadRequest")
        # handle malformed requests - read more below!
    except TimedOut:
        logger.warning(f"Error TimedOut")
        # handle slow connection problems
    except NetworkError:
        logger.warning(f"Error NetworkError")
        # handle other connection problems
    except ChatMigrated as e:
        logger.warning(f"Error ChatMigrated")
        # the chat_id of a group has changed, use e.new_chat_id instead
    except TelegramError:
        logger.warning(f"Error TelegramError")
        # handle all other telegram related errors
    except:
        context.bot.send_message(
            parse_mode = 'Markdown',
            chat_id = update.message.chat_id,
            text = "⚠ Ha ocurrido un error, por favor reportar lo sucedo a nuestro correo (utilizar */contacto*)"
        )
        logger.warning('An unknown error has occurred')

if __name__ == '__main__':
    updater = Updater(TOKEN, use_context = True)
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('informacion', choice_type_information))
    dispatcher.add_handler(CallbackQueryHandler(call_back_type_information, pattern='info'))
    dispatcher.add_handler(CommandHandler('total', summary))
    dispatcher.add_handler(CommandHandler('estados', state))
    dispatcher.add_handler(CommandHandler('apoyo', support))
    dispatcher.add_handler(CallbackQueryHandler(support_call_back, pattern='support'))
    dispatcher.add_handler(CommandHandler('contacto', contact))
    dispatcher.add_handler(MessageHandler(Filters.command, unknown))
    dispatcher.add_error_handler(error_handler)

    updater.start_polling()
    updater.idle()