apis_by_countrys = [
    {
        "country": 'Colombia',
        "slug": "colombia",
        "flag": '🇨🇴',
        "api_url": '',
        "fields": {
            "summary": {},
            "state": {}
        },
        "unique": False
    },
    {
        "country": 'Costa Rica',
        "slug": "costa-rica",
        "flag": '🇨🇷',
        "api_url": '',
        "fields": {
            "summary": {},
            "state": {}
        },
        "unique": False
    },
    {
        "country": 'España',
        "slug": "espana",
        "flag": '🇪🇸',
        "api_url": 'https://api.apify.com/v2/key-value-stores/lluBbYoQVN65R3BGO/records/LATEST?disableRedirect=true',
        "fields": {
            "summary": {
                "confirmed": 'infected',
                "recovered": 'recovered',
                "deaths": 'deceased'
            },
            "state": {
                # "state_or_province": 'Provincias',
                # "container": 'regions',
                # "state_name": 'region',
                # "total_cases": 'total',
                # "jump_position": 0
            }
        },
        "unique": True
    },
    {
        "country": 'Italia',
        "slug": "italia",
        "flag": '🇮🇹',
        "api_url": 'https://api.apify.com/v2/key-value-stores/UFpnR8mukiu0TSrb4/records/LATEST?disableRedirect=true',
        "fields": {
            "summary": {
                "confirmed": 'totalCases',
                "recovered": 'dischargedHealed',
                "deaths": 'deceased'
            },
            "state": {}
        },
        "unique": True
    },
    {
        "country": 'México',
        "slug": "mexico",
        "flag": '🇲🇽',
        "api_url": '',
        "fields": {
            "summary": {},
            "state": {}
        },
        "unique": False
    },
    {
        "country": 'Perú',
        "slug": "peru",
        "flag": '🇵🇪',
        "api_url": '',
        "fields": {
            "summary": {},
            "state": {}
        },
        "unique": False
    },
    {
        "country": 'Venezuela',
        "slug": "venezuela",
        "flag": '🇻🇪',
        "api_url": 'https://covid19.patria.org.ve/api/v1/summary/',
        "fields": {
            "summary": {
                "confirmed": 'Confirmed',
                "recovered": 'Recovered',
                "deaths": 'Deaths'
            },
            "state": {
                "state_or_province": 'Estados',
                "container": 'Confirmed'
            }
        },
        "unique": True
    }
]